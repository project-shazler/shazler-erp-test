# -*- coding: utf-8 -*-
{
    'name': "Shazler ERP Test",
    'summary': """Duplicate order product warning""",
    'author': "Taieb Nourddine",
    'category': 'Sales/Sales',
    'depends': ['sale'],
    'data': [
        'security/group_manage_price.xml',
  ]

}