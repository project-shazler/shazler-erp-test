from odoo import models, api, _


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.onchange('order_line')
    def check_duplicated_products(self):
        if self.env.user.has_group("shazler_erp_test.group_manage_prices"):
            product = []
            duplicated_products = []
            for line in self.order_line:
                if line.product_id in product:
                    duplicated_products.append(line.product_id)
                else:
                    product.append(line.product_id)
            duplicated_products = map(lambda l: l.name, duplicated_products)
            if duplicated_products:
                join_list_products = ", ".join(duplicated_products)

                message = _("these product is created in duplicate " + join_list_products)
                title = _("Warning for %s") % self.name
                warning = {
                    'title': title,
                    'message': message,
                }
                return {'warning': warning}
